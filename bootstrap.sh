GIT_DIR=`pwd`

echo "Copy .vimrc from $GIT_DIR to ~/"
cd ~/
rm -rf .vimrc
ln -s $GIT_DIR/.vimrc

echo "Create ~/.vim/autoload and ~/.vim/bundle folder and prepare autoload"
cd ~/
rm -rf .vim/
mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

echo "Clone NerdTree repository"
cd ~/.vim/bundle
git clone https://github.com/scrooloose/nerdtree.git
