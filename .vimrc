execute pathogen#infect()

set nu
set paste
syntax on
autocmd BufWritePre * :%s/\s\+$//e
set ts=2 sts=2 noet
set shiftwidth=2
set expandtab
set autoindent
retab

nmap <C-t> :tabnew<CR>
nmap <C-n> :NERDTreeToggle<CR>
nmap <C-h> <C-w>h
nmap <C-l> <C-w>l
